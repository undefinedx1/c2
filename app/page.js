import React from "react";
import {objectToRows, objectToColumnsInfo, objectToPaymentsRepo, objectToColumnsPayments} from "../components/objectToDataGridFormat"
import DataGrids from "../components/DataGrids";

export async function getInfo(){
    const info = fetch("https://www.baron.miami/api", { next: { revalidate: 1 } }).then(x => x.json())
    return info
}

export async function getPayments(){
    const payments = fetch("https://www.baron.miami/api/payments", { next: { revalidate: 1 } }).then(x => x.json())
    return payments
}

export default async function Page(){
    const info = await getInfo()
    const payments = await getPayments()

    return(
            <>
            <DataGrids
                infoRows = {objectToRows(info)}
                infoColumns = {objectToColumnsInfo(info) }
                paymentsColumns = {objectToColumnsPayments(payments[Object.keys(payments)[0]])}
                paymentsRepo = {objectToPaymentsRepo(payments)}
            />
            </>
            )
}