export function objectToRows(data){
    let rows = []
    // Traverse the Object full of Properties
    for (let docID in data){
        // For each property
        let row = {id: docID}  // create a new row representing the property
        let propertyInfoCollection = data[docID]
        for(let documentName in propertyInfoCollection) { // traverse the info collection
            row[documentName] = propertyInfoCollection[documentName]
        }
        rows.push(row)
    }
    return rows
}

export function objectToPaymentsRepo(obj){
    let returnObj = {}
    for (let elem in obj){
        returnObj[elem] = objectToRows(obj[elem])
    }
    return returnObj
}

export function objectToColumnsInfo(obj) {
    let columns = [{"field": "id", headerName: "id", width:170, type:"string"} ]
    for (let docID in obj) {     // traverse all property keys in obj
        let info = obj[docID]
        for (let documentID in info) {   // traverse all documents in info
            // for each thing in the info collection
            let found = false // in the thing has a column
            for(let elem in columns) { // for each document in info, check to see if it has a column
                if (columns[elem].field === documentID) {
                    found = true
                }
            }
            if (!found) {
                if (documentID === "value") {
                    columns.push({field: documentID, headerName: documentID, width: 70, editable: true, type: "number"})
                } else if (documentID === "autopay"){
                    columns.push({field: documentID, headerName: documentID, width: 76, editable: false, type: "boolean"})
                } else if (documentID === "timing"){
                    columns.push({field: documentID, headerName: documentID, width: 90, editable: false, type: "number"})
                }else if (documentID === "name"){
                    columns.push({field: documentID, headerName: documentID, width: 170, editable: true, type: "string"})
                }else if (documentID === "emails"){
                    columns.push({field: documentID, headerName: documentID, width: 350, editable: true, type: "string"})
                }else if (documentID === "underDevelopment"){
                    columns.push({field: documentID, headerName: documentID, width: 187, editable: true, type: "boolean"})
                }else if (documentID === "url"){
                    columns.push({field: documentID, headerName: documentID, width: 170, editable: true, type: "string"})
                }else if (documentID === "applicationsOpen"){
                    columns.push({field: documentID, headerName: documentID, width: 120, editable: true, type: "boolean"})
                }else if (documentID === "price"){
                    columns.push({field: documentID, headerName: documentID, width: 70, editable: true, type: "number"})
                }else if (documentID === "documents"){
                    break;
                }
                else {
                    columns.push({field: documentID, headerName: documentID, width: 125, editable: true})
                }
            }
        }
    }

    columns = columns.sort((a, b) => {return a.field.charCodeAt(0) - b.field.charCodeAt(0)})
    columns.unshift({field: "documents", headerName: "documents", width: 90, editable: false, type: "actions"})
    return columns;
}


export function objectToColumnsPayments(obj) {
    let columns = [{"field": "id", headerName: "id", width:170, type:"string"}]
    for (let docID in obj) {     // traverse all property keys in obj
        let info = obj[docID]
        for (let documentID in info) {   // traverse all documents in info
            // for each thing in the info collection
            let found = false // in the thing has a column
            for(let elem in columns) { // for each document in info, check to see if it has a column
                if (columns[elem].field === documentID) {
                    found = true
                }
            }
            if (!found) {
                if (documentID === "propertyID") {
                    columns.push({field: documentID, headerName: documentID, width: 170, editable: false, type: "number"})
                }else if (documentID === "status"){
                    columns.push({field: documentID, headerName: documentID, width: 80, editable: false, type: "string"})
                }else if (documentID === "name"){
                    columns.push({field: documentID, headerName: documentID, width: 70, editable: false, type: "string"})
                }else if (documentID === "amount"){
                    columns.push({field: documentID, headerName: documentID, width: 70, editable: false, type: "number"})
                }
                else if ((documentID !== "dateAdded") && (documentID !== "datesProcessing")) {
                    if (documentID !== "datePaid"){
                        columns.push({field: documentID, headerName: documentID, width: 125, editable: true})
                    }
                }
            }
        }
    }
    columns = columns.sort((a, b) => {return b.field.charCodeAt(0) - a.field.charCodeAt(0)})
    columns.push({field: "documents", headerName: "documents", width: 90, editable: false, type: "actions"})
    columns.push({field: "dateAdded", headerName: "dateAdded", width: 175, editable: false, type: "dateTime"})
    columns.push({field: "datesProcessing", headerName: "datesProcessing", width: 185, editable: false, type: "string"})
    columns.push({field: "datePaid", headerName: "datePaid", width: 175, editable: false, type: "dateTime"})
    return columns;
}