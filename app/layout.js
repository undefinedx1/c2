import "./globals.css"
import React from "react";
import localFont from '@next/font/local';
const myFont = localFont({ src: 'GoldmanSans_Th.ttf' });

export default function RootLayout({children}) {
    return (
            <html lang="en" className={myFont.className}>
                <head/>
                <body>
                        {children}
                </body>
            </html>
            )
}