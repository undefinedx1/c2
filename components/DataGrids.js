"use client";
import {DataGridPremium, GridActionsCellItem, GridToolbar} from "@mui/x-data-grid-premium";
import {Box} from "@mui/material";
import React from "react";
import {doc, updateDoc, onSnapshot} from "firebase/firestore";
import {db, units} from "../public/firebase";
import {objectToRows} from "../components/objectToDataGridFormat"
import {createTheme, ThemeProvider} from "@mui/material/styles";
import CloudQueueIcon from '@mui/icons-material/CloudQueue';
import TerminalOutlined from "@mui/icons-material/TerminalOutlined";

export default function DataGrids(props){

    const [infoRows, setInfoRows] = React.useState(props.infoRows)
    const [infoCols] = React.useState(hydrateInfoCols(props.infoColumns))
    const [paymentsRows, setPaymentsRows] = React.useState([])
    const [paymentsCols] = React.useState(hydratePaymentsCols(props.paymentsColumns))

    function hydrateInfoCols(arry){
        arry[0]["getActions"] = (x) =>  [<GridActionsCellItem key={"dd"} icon={<CloudQueueIcon/>} label={"copy"} onClick={copyGCPConsoleLinkInfo(x)}/>,<GridActionsCellItem key={"fg"} icon={<TerminalOutlined/>} label={"copy"} onClick={copyGCPTerminalLinkInfo(x)}/>]
        return arry
    }

    function hydratePaymentsCols(arry){
        arry[arry.length-1]["valueGetter"] = ({ value }) => value && new Date(value.seconds*1000)
        arry[arry.length-2]["valueGetter"] = ({ value }) => value && parseDatesProcessing(value)
        arry[arry.length-3]["valueGetter"] =  ({ value }) => value && new Date(value.seconds*1000)
        arry[arry.length-4]["getActions"] = (x) =>  [<GridActionsCellItem key={"zd"} icon={<CloudQueueIcon/>} label={"copy"} onClick={copyGCPConsoleLinkPayments(x)}/>,<GridActionsCellItem key={"2z"} icon={<TerminalOutlined/>} label={"copy"} onClick={copyGCPTerminalLinkPayments(x)}/>]
        return arry
    }

    React.useEffect(()=> {
        const unsubscribe = onSnapshot(units, (querySnapshot) => {
            const info = {};
            querySnapshot.forEach((doc) => {
                info[doc.id] = doc.data()
            });
            setInfoRows(objectToRows((info)))
        });
        return () => {unsubscribe()}
    }, [])

    function onSelectionModelChange(x){
        let newRows2 = []
        for (let elem in x){
            if (Object.keys(props.paymentsRepo).includes(x[elem]) ){
                let propertyID = x[elem]
                newRows2 = newRows2.concat(props.paymentsRepo[propertyID])
            }
        }
        setPaymentsRows(newRows2)
    }

    async function processRowUpdate(newCellState, oldCellState){
        return new Promise(async function(resolve, reject) {
            for(let key in newCellState){
                if (newCellState[key] !== oldCellState[key]) {
                    const docRef = doc(db, "units/" + newCellState.id);
                    let update = { }
                    if ((typeof newCellState[key] === "string")&&(newCellState[key].indexOf(",") !== -1)) {
                        newCellState[key] = newCellState[key].split(",")
                    }
                    update[key] = newCellState[key]
                    await updateDoc(docRef, update).then((x) => resolve(newCellState))
                }
            }
            resolve(oldCellState)
        })
    }

    function getDetailPanelContent(x){
        return(<h1>{x.id}</h1>)
    }

    const copyGCPConsoleLinkInfo = React.useCallback((row)=> () => {
        let formattedPropertyID = row.id.replaceAll(" ", "-")
        formattedPropertyID = formattedPropertyID.toLowerCase()
        navigator.clipboard.writeText("https://console.cloud.google.com/storage/browser/" + formattedPropertyID)
    },[])

    const copyGCPTerminalLinkInfo = React.useCallback((row)=> ()=> {
        let formattedPropertyID = row.id.replaceAll(" ", "-")
        formattedPropertyID = formattedPropertyID.toLowerCase()
        navigator.clipboard.writeText("gs://" + formattedPropertyID)
    },[])

    const copyGCPConsoleLinkPayments = React.useCallback((row)=> ()=> {
        let formattedPropertyID = row.row.propertyID.replaceAll(" ", "-")
        formattedPropertyID = formattedPropertyID.toLowerCase()
        let formattedPaymentName = row.row.name.replaceAll(" ", "-")
        formattedPaymentName = formattedPaymentName.toLowerCase()
        navigator.clipboard.writeText("https://console.cloud.google.com/storage/browser/cyberneticstream/" + formattedPropertyID + "-" + formattedPaymentName)
    },[])
    const copyGCPTerminalLinkPayments = React.useCallback((row)=> ()=> {
        let formattedPropertyID = row.row.propertyID.replaceAll(" ", "-")
        formattedPropertyID = formattedPropertyID.toLowerCase()
        let formattedPaymentName = row.row.name.replaceAll(" ", "-")
        formattedPaymentName = formattedPaymentName.toLowerCase()
        navigator.clipboard.writeText("gs://cyberneticstream/" + formattedPropertyID + "-" + formattedPaymentName)
    },[])

    return(
            <>
            <ThemeProvider theme={theme}>
                <Box sx={{height:"100vh", width:"100%"}}>
                    <DataGridPremium
                        rows={infoRows}
                        columns={infoCols}
                        initialState={infoGridInitalState}
                        onSelectionModelChange={(x) => onSelectionModelChange(x)}
                        processRowUpdate={processRowUpdate}
                        components={{Toolbar: GridToolbar}}
                        checkboxSelection
                        density ="compact"
                        experimentalFeatures={{aggregation: true, newEditingApi: true}}
                        sx={{
                        '@media print': {
                            '.MuiDataGrid-main': { color: 'rgb(0, 0, 0)' },
                        },
                        }}
                    />
                </Box>

                <Box sx={{height:"100vh", width:"100%"}}>
                    <DataGridPremium
                        rows={paymentsRows}
                        columns={paymentsCols}
                        initialState = {paymentsGridInitalState}
                        components={{Toolbar: GridToolbar}}
                        density ="compact"
                        experimentalFeatures={{aggregation: true}}
                        sx={{
                        '@media print': {
                            '.MuiDataGrid-main': { color: 'rgb(0, 0, 0)' },
                        },
                        }}
                    />
                </Box>
            </ThemeProvider>
            </>
            )
}

const theme = createTheme({
    palette: {
        primary: {
            main: "#000000",
        }
    },
    typography: {
        fontFamily: "var(--font-inter)",
    }
});

const infoGridInitalState = {
    columns: {
        columnVisibilityModel: {
            autopay: false,
            emails: false,
            timing: false,
            coordinates: false,
            fullAddress:false,
            url: false,
            name: false,
            applicationsOpen: false,
            documents: false,
            underDevelopment: false,
            "__detail_panel_toggle__": true,
            "__reorder__": false,
            "__check__": true
        },
    },
    aggregation: {
        model:{
            value: "sum",
            price: "sum"
        }
    },
    filter: {
        filterModel: {
                items: [{columnField: "underDevelopment", operatorValue: "is", value: "false", id:47990}], linkOperator: "and", quickFilterLogicOperator: "and", quickFilterValues: []
        }
    },
}

const paymentsGridInitalState = {
    columns: {
        columnVisibilityModel: {
            id: false,
            dateAdded: false,
            datesProcessing: false,
            datePaid: false,
            paid: false,
            documents: false,
            "__detail_panel_toggle__": false,
            "__reorder__": false,
            "__check__": true
        },
    },
    aggregation: {
        model:{
            amount: "sum",
        }
    },
    sorting: {
        sortModel: [{ field: "status", sort: "desc" }]
    },
    filter: {
        filterModel: {
            items: [{columnField: "name", operatorValue: "isAnyOf", id:93596}, {columnField: "status", operatorValue: "isAnyOf", id:93595}], linkOperator: "and", quickFilterLogicOperator: "and", quickFilterValues: []
        }
    },
    pinnedColumns: {
        left: ["propertyID"]
    }
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = ampm;
    return strTime;
}

function parseDatesProcessing(value){
    let returnStr = ""
    for (let elem in value) {
        let date = new Date(value[elem].seconds * 1000)
        returnStr += elem + ": " + date.getMonth()+"/"+date.getDate()+"/"+date.getFullYear() + ", " + date.getHours()+":"+date.getMinutes()+":" + date.getSeconds() + " " + formatAMPM(date) + " "
    }
    return returnStr
}